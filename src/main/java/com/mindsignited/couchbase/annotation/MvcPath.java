package com.mindsignited.couchbase.annotation;

import java.lang.annotation.*;

/**
 * Created by nicholaspadilla on 2/6/15.
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface MvcPath {
}

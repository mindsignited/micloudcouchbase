package com.mindsignited.couchbase.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.data.couchbase.core.mapping.Document;
import org.springframework.data.couchbase.core.mapping.Field;

import java.io.Serializable;
import java.util.UUID;


/**
 * @author nicholas
 *
 */
@Getter
@Setter
@Document
public class Sensor implements Serializable {

	private static final long serialVersionUID = 1752377420492878723L;

    @Id
    private String id = UUID.randomUUID().toString();
    @Version
    private Long version;
    @Field
	private String label;
    @Field
	private String mac;
    @Field
    @JsonIgnore
    private String uid;
    @Field
    private String activationCode;
    @Field
    private String serial;
    @Field
    private String modelName;
    @Field
    private String productName;
    @Field
    private String authenticationKey;
    @Field
    private String deviceName;
    @Field
    private String deviceType;
    @Field
    private String deviceMode;
    @Field
    private String fullFirmwareVersion;
    @Field
    private String versionDescription;
    @Field
    private String bootloaderVersion;
    @Field
    private String kernelDf;
    @Field
    private String rootfsDf;
    @Field
    private String microProcessorVersion;
    @Field
    private String ubl;
    @Field
    private String revision;
    @Field
    private String userfsVersion;
    @Field
    private String hardwareVersion;
    @Field
    private String hardwareRevision;
    @Field
    private String safe;
    @Field
    private String manufacturer;
    @Field
    private String fullname;
    @Field
    private String shortname;
    @Field
    private String description;
    @Field
    private String kernelVersion;
    @Field
    private String rootFs;
    @Field
    private Boolean sdCardUsable;
    @Field
    private Boolean lte;
    @Field
    private Boolean wifi;
    @JsonIgnore
    @Field
    private SensorStatusType status;


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return this.label + " MAC: " + this.mac;
	}

}

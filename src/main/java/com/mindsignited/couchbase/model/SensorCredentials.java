package com.mindsignited.couchbase.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.data.couchbase.core.mapping.Document;
import org.springframework.data.couchbase.core.mapping.Field;

import java.io.Serializable;
import java.util.UUID;

/**
 * All of the various credential information to be used by the camera.
 * @author Navid
 *
 */
@Getter
@Setter
@Document
public class SensorCredentials implements Serializable {

	@Id
    private String id = UUID.randomUUID().toString();
	@Version
	private Long version;
    @Field
	private Sensor sensor;
    @Field
	private String uuid;
    @Field
	private String password;
    @Field
	private String sharedSecret;

}

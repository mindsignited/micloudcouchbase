package com.mindsignited.couchbase.model;

/**
 * @author nicholas
 *
 */
public enum SensorStatusType {
	ORPHAN,
	ADOPTED,
	SUSPENDED
}

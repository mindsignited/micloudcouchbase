package com.mindsignited.couchbase.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.data.couchbase.core.mapping.Document;
import org.springframework.data.couchbase.core.mapping.Field;

import java.util.UUID;


/**
 * Created by nicholaspadilla on 2/6/15.
 */
@Getter
@Setter
@Document
public class Permission {

    @Id
    private String id = UUID.randomUUID().toString();
    @Version
    private Long version;
    @Field
    private String resourceName;
    @Field
    private String resourceId;
}

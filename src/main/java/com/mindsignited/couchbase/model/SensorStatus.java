package com.mindsignited.couchbase.model;

import java.io.Serializable;
import java.util.Date;

/**
 * @author nicholaspadilla
 *
 */
public class SensorStatus implements Serializable {

	private boolean liveStreamStatusConnected;
	private Date liveStreamStatusLastConnected;
	private Date liveStreamStatusLastDisconnected;
	
	private boolean messagingStatusConnected;
	private Date messagingStatusLastConnected;
	private Date messagingStatusLastDisconnected;
	
	
	/**
	 * @return the liveStreamStatusConnected
	 */
	public boolean isLiveStreamStatusConnected() {
		return liveStreamStatusConnected;
	}
	/**
	 * @param liveStreamStatusConnected the liveStreamStatusConnected to set
	 */
	public void setLiveStreamStatusConnected(boolean liveStreamStatusConnected) {
		this.liveStreamStatusConnected = liveStreamStatusConnected;
	}
	/**
	 * @return the liveStreamStatusLastConnected
	 */
	public Date getLiveStreamStatusLastConnected() {
		return liveStreamStatusLastConnected;
	}
	/**
	 * @param liveStreamStatusLastConnected the liveStreamStatusLastConnected to set
	 */
	public void setLiveStreamStatusLastConnected(Date liveStreamStatusLastConnected) {
		this.liveStreamStatusLastConnected = liveStreamStatusLastConnected;
	}
	/**
	 * @return the liveStreamStatusLastDisconnected
	 */
	public Date getLiveStreamStatusLastDisconnected() {
		return liveStreamStatusLastDisconnected;
	}
	/**
	 * @param liveStreamStatusLastDisconnected the liveStreamStatusLastDisconnected to set
	 */
	public void setLiveStreamStatusLastDisconnected(
			Date liveStreamStatusLastDisconnected) {
		this.liveStreamStatusLastDisconnected = liveStreamStatusLastDisconnected;
	}
	/**
	 * @return the messagingStatusConnected
	 */
	public boolean isMessagingStatusConnected() {
		return messagingStatusConnected;
	}
	/**
	 * @param messagingStatusConnected the messagingStatusConnected to set
	 */
	public void setMessagingStatusConnected(boolean messagingStatusConnected) {
		this.messagingStatusConnected = messagingStatusConnected;
	}
	/**
	 * @return the messagingStatusLastConnected
	 */
	public Date getMessagingStatusLastConnected() {
		return messagingStatusLastConnected;
	}
	/**
	 * @param messagingStatusLastConnected the messagingStatusLastConnected to set
	 */
	public void setMessagingStatusLastConnected(Date messagingStatusLastConnected) {
		this.messagingStatusLastConnected = messagingStatusLastConnected;
	}
	/**
	 * @return the messagingStatusLastDisconnected
	 */
	public Date getMessagingStatusLastDisconnected() {
		return messagingStatusLastDisconnected;
	}
	/**
	 * @param messagingStatusLastDisconnected the messagingStatusLastDisconnected to set
	 */
	public void setMessagingStatusLastDisconnected(
			Date messagingStatusLastDisconnected) {
		this.messagingStatusLastDisconnected = messagingStatusLastDisconnected;
	}
}

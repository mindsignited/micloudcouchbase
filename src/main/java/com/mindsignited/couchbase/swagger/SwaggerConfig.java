package com.mindsignited.couchbase.swagger;

import com.mangofactory.swagger.configuration.SpringSwaggerConfig;
import com.mangofactory.swagger.models.dto.ApiInfo;
import com.mangofactory.swagger.plugin.EnableSwagger;
import com.mangofactory.swagger.plugin.SwaggerSpringMvcPlugin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableSwagger
public class SwaggerConfig {

    @Value("${swagger.includePatterns:/*.*?}")
    private String includePatterns;

    @Value("${swagger.apiInfo.title:}")
    private String title;
    @Value("${swagger.apiInfo.description:}")
    private String description;
    @Value("${swagger.apiInfo.termsOfServiceUrl:}")
    private String termsOfServiceUrl;
    @Value("${swagger.apiInfo.contact:}")
    private String contact;
    @Value("${swagger.apiInfo.license:}")
    private String license;
    @Value("${swagger.apiInfo.licenseUrl:}")
    private String licenseUrl;

    @Autowired
    private SpringSwaggerConfig springSwaggerConfig;


    @Bean
    public SwaggerSpringMvcPlugin customImplementation() {
        return new SwaggerSpringMvcPlugin(this.springSwaggerConfig)
                .apiInfo(apiInfo())
                .includePatterns(includePatterns.split(","));
    }

    /**
     * API Info as it appears on the swagger-ui page
     */
    private ApiInfo apiInfo() {
        return new ApiInfo(title, description, termsOfServiceUrl, contact, license, licenseUrl);
    }

}

package com.mindsignited.couchbase.repository;

import com.mindsignited.couchbase.model.Sensor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.transaction.annotation.Transactional;


@Transactional(readOnly = true)
@RepositoryRestResource(collectionResourceRel = "sensors", path = "sensors")
public interface SensorRepository extends CrudRepository<Sensor, String> {

    @Transactional
    public Sensor findByMac(String mac);

    @Transactional
    public Sensor findByMacAndUid(String mac, String uid);

}

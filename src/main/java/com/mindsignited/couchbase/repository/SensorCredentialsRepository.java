package com.mindsignited.couchbase.repository;

import com.mindsignited.couchbase.model.Sensor;
import com.mindsignited.couchbase.model.SensorCredentials;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "sensorcredentials", path = "sensorcredentials")
public interface SensorCredentialsRepository extends CrudRepository<SensorCredentials, String> {

    public SensorCredentials findByUuid(String uuid);

    public SensorCredentials findBySensor(Sensor sensor);

    public SensorCredentials findBySensor_Mac(String mac);

}

package com.mindsignited.couchbase.repository;

import com.mindsignited.couchbase.model.Permission;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Created by nicholaspadilla on 2/6/15.
 */
@RepositoryRestResource(collectionResourceRel = "permissions", path = "permissions")
public interface PermissionRepository extends CrudRepository<Permission, String> {
}

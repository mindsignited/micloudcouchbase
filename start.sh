#!/bin/bash

## MI Cloud Sensor Service

export EUREKA_SERVER_URI=http://localhost:8761/micloudeureka
export SERVER_PORT=8100

export KEYCLOAK_BASE_URL=http://127.0.0.1:8080/auth
export KEYCLOAK_REALM=micloud

if [ "$1" == "debug" ]; then
    mvn spring-boot:run -Drun.jvmArguments="-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=5011" --debug
else
    mvn spring-boot:run $@
fi

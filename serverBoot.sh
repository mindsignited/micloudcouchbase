#!/bin/bash

##
## Startup script for REST API Server Base.  This will set the needed environment variables and
## boot up the application using the spring boot fat jar that is the artifact of the
## production build.
##

CONFIG_APPLICATION=mi-cloud-couch-base
CONFIG_APPLICATION_EXTENSION=jar
CONFIG_VERSION=0.0.1-SNAPSHOT
PATH_TO_EXECUTABLE=target # <-- no trailing slash here.
# jvm arguments added here
JVM_OPTS="" #"-Dspring.profiles.active=production"

export SERVER_PORT=8100
export EUREKA_SERVER_URI=http://127.0.0.1:8761/micloudeureka
export HOSTNAME=127.0.0.1

export KEYCLOAK_BASE_URL=http://127.0.0.1:8080/auth
export KEYCLOAK_REALM=micloud


java ${JVM_OPTS} -jar ${PATH_TO_EXECUTABLE}/${CONFIG_APPLICATION}-${CONFIG_VERSION}.${CONFIG_APPLICATION_EXTENSION}
